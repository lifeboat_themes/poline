<div class="all-hyperion-page">
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                <div class="product-simple-area ptb-80">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="tab-content">
                                <% loop $Media %>
                                    <div class="tab-pane show <% if $First %> active <% end_if %>" id="view$ID">
                                        <a class="image-link" href="$Fill(800, 1000).AbsoluteLink"><img src="$Fill(800, 1000).AbsoluteLink" alt="$Title"/></a>
                                    </div>
                                <% end_loop %>
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav sinple-tab-menu" role="tablist">
                                <% loop $Media.limit(5) %>
                                    <li><a class="ml-0 <% if $First %> active <% end_if %>" href="#view$ID" data-toggle="tab"><img style="background: white;" src="$Fill(79, 99).AbsoluteLink" alt="$Title" /></a></li>
                                <% end_loop %>
                            </ul>
                        </div>
                        <div class="col-md-5">
                            <div class="product-simple-content">
                                <div class="sinple-c-title">
                                    <h3>$Title</h3>
                                </div>
                                <% if $SKU %>
                                    <span> SKU: $SKU</span>
                                <% end_if %>
                                <div class="product-details" data-role="product-form" data-id="$ID" data-url="$AbsoluteLink" data-form="#product-form">
                                    <div id="product-form"></div><br/>
                                    <div class="quick-add-to-cart">
                                        <form method="post" class="cart">
                                            <div class="row">
                                                <div class="col-lg-3 product-form product-qty numbers-row mr-2 mb-3">
                                                    <label for="french-hens">Qty:</label>
                                                    <input class="quantity" type="number" min="1" max="1000" step="1" id="Quantity" value="1" />
                                                </div>
                                                <div class="col-lg-6 cart_btn prod-btn">
                                                    <button class="ml-1 mb-2 single_add_to_cart_button add-to-cart hyper-page" type="submit"><span class="lnr lnr-cart"> </span>Add to cart</button>
                                                </div>
                                                <div class="col-lg-2 action-heiper prod-btn">
                                                    <% include WishlistButtonIcon Product=$Me %>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <hr/>
                                <p><% if $Summary %>$Summary<% else %>$Description.Summary<% end_if %></p>
                            </div>
                        </div>
                    </div>
                </div>

                <% if $Description || $Descriptions %>
                    <div class="product-info-detailed pb-80">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="product-info-tab">
                                    <ul class="nav product-info-tab-menu" role="tablist">
                                        <% if $Description %>
                                            <li><a class="active" href="#desc" data-toggle="tab">Description</a></li>
                                        <% end_if %>
                                        <% if $Descriptions %>
                                            <% loop $Descriptions %>
                                                <li><a href="#desc$ID" data-toggle="tab">$Title</a></li>
                                            <% end_loop %>
                                        <% end_if %>
                                    </ul>
                                    <div class="tab-content">
                                        <% if $Description %>
                                            <div class="tab-pane active" id="desc">
                                                <div class="product-info-tab-content">
                                                    $Description
                                                </div>
                                            </div>
                                        <% end_if %>
                                        <% loop $Descriptions %>
                                            <div class="tab-pane" id="desc$ID">
                                                <div class="product-info-tab-content">
                                                    $Text
                                                </div>
                                            </div>
                                        <% end_loop %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <% end_if %>
                <% if $SiteSettings.EnableReviews %>
                    <div>
                        <h2 class="title pb-3 pl-3">Reviews</h2>
                        <div class="row pl-3 mt-4 pb-4">
                            <div class="col">
                                $ReviewSection()
                            </div>
                        </div>
                    </div>
                <% end_if %>
                <br/>
                <% if $RelatedProducts.count %>
                    <div class="upsell-product">
                        <div class="upsell-product-title">
                            <h3 class="text-uppercase">Related Products</h3>
                        </div>
                        <div class="row dotted-style3">
                            <div class="upsell-product-active">
                                <% loop $RelatedProducts(5) %>
                                    <% include ProductCard Product=$Me %>
                                <% end_loop %>
                            </div>
                        </div>
                    </div>
                <% end_if %>
            </div>
        </div>
    </div>
</div>