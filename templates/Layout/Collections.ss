<div class="container">
    <section class="mt-md-10 pt-md-3 mt-6 mb-md-10 mb-6">
        <div class="row">
            <% loop $AllCollections %>
            <div class="col-md-6">
                <div>
                    <a href="$AbsoluteLink">
                        <figure>
                            <img src="$Image.Fill(585,400).AbsoluteLink" alt="$Collection.Title">
                        </figure>
                    </a>
                    <div class="collection-info">
                        <h4>$Title</h4>
                        <span><span>$Products.count</span> Products </span>
                        <a href="$AbsoluteLink" class="btn-link">Shop Now</a>
                    </div>
                </div>
            </div>
            <% end_loop %>
            <div class="col-1"></div>
        </div>
    </section>
</div>