<div class="single-new-product">
    <div class="product-img">
        <a href="$Product.AbsoluteLink">
            <% if $Image %>
                <img src="$Product.Image.Fill(800,1000).Link" class="first_img" alt="$Product.Title" />
            <% else %>
                <img src="$SiteSettings.Logo.Fill(800,1000).Link" class="first_img" alt="$Product.Title" />
            <% end_if %>
        </a>
    </div>
    <div class="product-content text-center">
        <a href="$Product.AbsoluteLink"><h3>$Product.Title</h3></a>
        <% if $Product.Variants.count %>
            <% if $Product.MinPrice != $Product.MaxPrice %>
                <h4>$Product.FormatPrice('MinPrice') - $Product.FormatPrice('MaxPrice')</h4>
            <% else %>
                <h4>$Product.FormatPrice('MinPrice')</h4>
            <% end_if %>
        <% else %>
            <% if $Product.isDiscounted %>
                <h3 class="del-price"><del>$Product.FormatPrice('BasePrice')</del></h3>
                <h4> $Product.FormatPrice('SellingPrice')</h4>
            <% else %>
                <h4> $Product.FormatPrice('BasePrice')</h4>
            <% end_if %>
        <% end_if %>
    </div>
</div>