<% if $Top.WishList.Products.byID($Product.ID) %>
    <a href="#" title="Remove $Product.Title to your wish list" data-id="$Product.ID" class="btn-product-icon btn-wishlist wishlist-toggle">
        <span class="lnr lnr-heart"></span>
    </a>
<% else %>
    <a href="#" title="Add $Product.Title to your wish list" data-id="$Product.ID" class="btn-product-icon btn-wishlist wishlist-toggle">
        <span class="lnr lnr-heart"></span>
    </a>
<% end_if %>