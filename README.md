#Poline

##Home Page

###Slider Collections
You may have up to 4 collections that are used in a slider on the home page. Choose up to four collections to be shown, in the custom Fields labelled
**Home: Slider Collection 1,
Home: Slider Collection 2,
Home: Slider Collection 3,
Home: Slider Collection 4.**

_You may also change the image shown on the banners by setting the image in the field labelled; **Slider Image**_

_NOTE:
Slider Collection 1 and Slider Collection 2 are shown as the first things on the home page, whilst Slider Collection 3 and Slider Collection 4 are shown further down the page_

###Banner Collections
Next to the Slider Collection 1 and Slider Collection 2 there can be 2 collections set, at the top of the home page.
These collections can be set in the custom fields **Home: Banner Collection 1, Home: Banner Collection 2.**

###Carousel Collections
Under the Slider and Banner section, you may have 2 collections to be featured in different carousels. To add these select the collections from the custom fields labelled **Home: Carousel Collection 1, Home: Carousel Collection 2**

_NOTE: Carousel Collection 1 is the left-most carousel, whilst Carousel Collection 2 is the right-most carousel which also has a banner with a heading underneath it._

To add the banner image and heading:
Select the image in the custom field labelled: Carousel Section Banner Image (Recommended size 770x150) and
Input the heading in the custom field labelled: Carousel Section Banner Heading

###Featured Collection
Underneath the Carousel Section, you can have a featured collection with products being displayed in a carousel. This collection can be added by selecting the collection from the 
custom field labelled **Home: Featured Collection**.

###Display Collection
The last section, shown before the footer and after the second Slider Section is the Display collection. This collection shows up to 15 products in a carousel with 3 columns of 3 products shown at one time
This collection is set from the custom field labelled **Home: Display Collection**.

##Footer
### Footer: First Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the left of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Second Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown in the middle of the three menu options.

_Note: Submenus items will not be displayed_

###Footer: Widget
A widget can be placed to be shown on the right-hand side of the footer, for example a map.
This is done by editing the HTML custom field labelled **Footer: Widget**

##Contact Page
###Map Location
In the Contact page, you can have a map of the store displayed underneath the Contact form.
This can be done by going to the custom field labelled **Map Location** in the Design section and inputting the store name and full address in the field, in the format shown below.

**Store Name, Street Name, Location Post Code**

_NOTE: It is important to get these details exactly as they are shown on Google Maps._

##Blog
###Blog page Banner Image
In the Blog Page where all blogs are shown, a custom image can be set to be shown in a banner.
This is set by selecting an image in the custom field labelled **Blog page Banner Image**.


##Collection
###Collection Heading
A collection may have a heading that is visible in the Slider Collections. This is done by typing in a Heading in the field labelled **Collection Heading** when editing the collection of choice.